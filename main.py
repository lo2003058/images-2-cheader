import os
from PIL import Image, ImageSequence


def process_image(image_path):
    with Image.open(image_path) as img:
        frames = [frame.copy().convert('RGBA') for frame in ImageSequence.Iterator(img)]
    return frames


def rgb888_to_rgb565(r, g, b):
    r5 = r >> 3
    g6 = g >> 2
    b5 = b >> 3
    rgb565 = (r5 << 11) | (g6 << 5) | b5
    return rgb565


# it makes transparent pixel to white pixel
# def frames_to_byte_arrays(frames):
#     byte_arrays = []
#     for frame in frames:
#         byte_array = bytearray()
#         for pixel in frame.getdata():
#             r, g, b, _ = pixel  # ignore alpha
#             rgb565_value = rgb888_to_rgb565(r, g, b)
#             byte_array.extend(rgb565_value.to_bytes(2, byteorder='big'))
#         byte_arrays.append(byte_array)
#     return byte_arrays


# it makes transparent pixel to black pixel
def frames_to_byte_arrays(frames):
    byte_arrays = []
    for frame in frames:
        byte_array = bytearray()
        for pixel in frame.getdata():
            r, g, b, a = pixel  # get alpha value
            if a == 0:  # if pixel is transparent
                r, g, b = 0, 0, 0  # set RGB to black
            rgb565_value = rgb888_to_rgb565(r, g, b)
            byte_array.extend(rgb565_value.to_bytes(2, byteorder='big'))
        byte_arrays.append(byte_array)
    return byte_arrays


def generate_header(byte_arrays, header_path, frame_count, width, height, image_name, is_gif):
    array_size = width * height  # Corrected array_size
    with open(header_path, 'w') as f:
        if is_gif:
            f.write(f'int {image_name}FramesNumber = {frame_count};\n')
        f.write(f'int {image_name}Width = {width};\n')
        f.write(f'int {image_name}Height = {height};\n\n')
        if is_gif:
            f.write(f'const unsigned short {image_name}[{frame_count}][{array_size}] PROGMEM = {{\n')
        else:
            f.write(f'const unsigned short {image_name}[{array_size}] PROGMEM = {{\n')

        for byte_array in byte_arrays:
            if is_gif:
                f.write('{')  # Open brace for each frame (or single frame if not a GIF)
            for i in range(0, len(byte_array), 2):  # Iterate by 2 as each pixel now takes 2 bytes
                value = (byte_array[i] << 8) | byte_array[i + 1]
                if i < len(byte_array) - 2:  # Check if it's not the last pixel pair
                    f.write(f'0x{value:04X}, ')
                else:
                    f.write(f'0x{value:04X}')  # No comma after the last value pair
            if is_gif:
                f.write('},\n')  # Close brace for each frame (or single frame if not a GIF)

        f.write('};\n')  # Close brace for the entire array


def convert_image_to_header(image_path, header_folder, image_name):
    frames = process_image(image_path)
    byte_arrays = frames_to_byte_arrays(frames)
    frame_count = len(frames)
    width, height = frames[0].size  # Assumes all frames have the same dimensions
    header_path = os.path.join(header_folder, f'{image_name}.h')
    is_gif = image_path.lower().endswith('.gif')
    generate_header(byte_arrays, header_path, frame_count, width, height, image_name, is_gif)


if __name__ == "__main__":
    source_folder = 'images'
    header_folder = 'cHeader'
    os.makedirs(header_folder, exist_ok=True)  # Create header_folder if it doesn't exist

    valid_extensions = ['.png', '.jpg', '.jpeg', '.gif']

    for root, dirs, files in os.walk(source_folder):
        for file in files:
            if any(file.lower().endswith(ext) for ext in valid_extensions):
                image_path = os.path.join(root, file)
                # Use the image file name (without extension) as the image_name
                image_name = os.path.splitext(file)[0]

                # Determine the subdirectory structure and create the corresponding subdirectories in header_folder
                sub_dir = os.path.relpath(root, source_folder)
                header_sub_folder = os.path.join(header_folder, sub_dir)
                os.makedirs(header_sub_folder, exist_ok=True)

                convert_image_to_header(image_path, header_sub_folder, image_name)
