# Image to C Header Converter

This script converts images (GIF, JPEG, PNG) to C header files. For GIF images, each frame is converted separately, and the resulting data is stored in a 2D array in the C header file. For JPEG and PNG images, the single image is converted, and the resulting data is stored in a 1D array in the C header file.

## Requirements

- Python 3.x
- Pillow library

## Installation

1. Install Python 3.x from the [official website](https://www.python.org/downloads/).
2. Install Pillow using pip:

```bash
pip install Pillow
```

## Structure of Generated C Header Files
For GIF images, the generated header file will contain the following:

framesNumber: An int variable indicating the number of frames in the GIF.
Width: An int variable indicating the width of the frames.
Height: An int variable indicating the height of the frames.
A 2D array containing the pixel data for each frame.
For JPEG and PNG images, the generated header file will contain the following:

Width: An int variable indicating the width of the image.
Height: An int variable indicating the height of the image.
A 1D array containing the pixel data.

## Pixel Format
The pixel data is stored in the RGB565 format, where each pixel is represented by a 16-bit value:

The red channel is represented by bits 15 to 11.
The green channel is represented by bits 10 to 5.
The blue channel is represented by bits 4 to 0.

## Contributing
Feel free to fork this repository, make changes, and submit pull requests. For major changes, please open an issue first to discuss the change.
